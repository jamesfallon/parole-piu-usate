# Parole Più Usate

Scan a text file to lookup the distribution of common/uncommon words in Italian
language.

## Installation

Requires python 3.9+ (older versions should be compatible if removing type
hinting from source code).

Current python package dependencies are:

  - pandas

Optional python package dependencies are:

  - matplotlib (for plotting functionality)
  - openpyxl (for excel export functionality)

## Usage

```
usage: lookup.py [-h] [-x] [-p] [-i] [--missing] [-q | -v | -d] fname

Parole Più Usate: Scan a text file to lookup the distribution of common/uncommon words in Italian language.

positional arguments:
  fname          filepath to input text

optional arguments:
  -h, --help     show this help message and exit
  -x, --excel    export to excel sheets
  -p, --plot     interactive plotting
  -i, --info     print summary info such as percentage of words identified
  --missing      (for debugging/improving the engine) save missing words list
  -q, --quiet
  -v, --verbose
  -d, --debug
```

## Development

### Roadmap

* user interface (desktop app or web app? tkinter? wxpython?)

* improved plotting and information output

* test against different difficulty texts (currently the difficulty ranking
  finds it hard to discriminate A1/B1/C1 texts, how to improve this metric?)

* expand word list (`parole.csv`) from top 1000 to 10,000. Note currently
  categorised by noun/verb/adjective/misc, whilst this isn't necessary it makes
  the end stats more interesting, easier to interpret. Could add a "not in the
  top 1000 category?"

### Update verb database

Only follow these instructions if modifying the `convert_json.py` process,
otherwise use the pre-processed verb mappings in `verb_mappings`

**Extract <https://github.com/ian-hamlin/verb-data/tree/master/json/italian>**
to the root folder `verb-data-italian`.

**Modify `convert_json.py`** with desired changes, if any (eg. add
functionality for indicative/negative indicative phrase splitting)

**Run `convert_json.py`**

This will populate folder `verb_mapping` with verb conjugation-infinitive
mappings sorted alphabetically (by infinitive).

## Acknowledgements

### Verb Database

Thanks to verb-data for a fantastic database of words in JSON format

<https://github.com/ian-hamlin/verb-data>

### Most used words

1000 most common words, sorted by noun/adjective/verb/misc

<http://telelinea.free.fr/italien/1000_parole.html>

### Article contractions

<https://grammar.collinsdictionary.com/italian-easy-learning>

## License

Creative Commons 3

https://creativecommons.org/licenses/by-sa/3.0/
