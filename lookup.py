"""Parole piu usate"""
import argparse
import logging
from glob import glob

import matplotlib.pyplot as plt
import pandas as pd
from pandas import DataFrame, Series


def strip_punctuation(word: str) -> str:
    """Remove punctuation and convert to lower case"""
    return word.strip(r",.!?:;'@{}[]()<>-=_+&#~/\|“”" + '"').lower()


def article_split(text_list: list[str], articles: DataFrame) -> list[str]:
    """Look for compound words to split"""
    new_list = list()
    for entry in text_list:
        if len(entry) > 2:
            if entry in articles.index:
                first, second = articles.loc[entry]
                new_list.append(first)
                entry = second
        new_list.append(entry)
    return new_list


def strip_generic(text_list: list[str], mappings: Series) -> list[str]:
    """Convert specific into generic (il -> lo, una -> uno, etc.)"""
    new_list = list()
    for entry in text_list:
        if entry in mappings.index:
            new_list.append(mappings.loc[entry].values[0])
        else:
            new_list.append(entry)
    return new_list


def strip_empty(text_list: list[str]) -> list[str]:
    """Remove empty entries and punctuation"""
    new_list = list()
    for entry in text_list:
        new_entry = entry.strip(r",.!?:;'@{}[]()<>-=_+&#~/\|“”" + '"')
        if len(new_entry) > 0:
            new_list.append(new_entry)
    return new_list


def make_list(generic: any) -> list:
    """convert any type into list if not already list"""
    if isinstance(generic, list):
        return generic
    return list(generic)


def split(text: str) -> list[str]:
    """Split text into words (careful of contractions)"""
    # split by spaces
    text_list = text.split(" ")

    # split up contractions
    new_list = list()
    for word in text_list:
        # check at least 3 letters long
        if len(word) > 2:
            # don't count start/end of word
            word_mid = word[1:-1]
            if "'" in word_mid:
                word_split = word_mid.split("'")
                # assert len(word_split) < 3, "only one contraction can be handled"
                # store first word in list. word=second word, added later
                new_list.append(word[0] + word_split[0])
                if len(word_split) > 2:
                    for word in make_list(word_split[1:-1]):
                        new_list.append(word)
                word = word_split[-1] + word[1]
        new_list.append(word)

    # remove punctuation
    new_list = list(map(strip_punctuation, new_list))

    return new_list


def rank(words_category, text_list):
    """Get word ranking of text_list from words"""
    word_list = words_category.to_list()
    word_set = set(words_category)
    rankings, records, lookup, index = list(), list(), list(), list()
    # loop over words in text
    for i, word in enumerate(text_list):
        if word in word_set:
            ranking = word_list.index(word)
            rankings.append(ranking)
            records.append(word)
            lookup.append(word)
            index.append(i)
    return rankings, records, lookup, index


def rank_verbs(verb_series, text_list, conjugations, infinitives):
    """Get verb ranking of text_list from words"""
    verbs = verb_series.to_list()
    conjugation_set = set(conjugations)
    infinitive_set = set(verbs)
    # loop over words in text
    rankings, records, lookup, index = list(), list(), list(), list()
    for i, word in enumerate(text_list):
        if word in conjugation_set:
            idx = conjugations.index(word)
            infinitive = infinitives[idx]
            if infinitive in infinitive_set:
                ranking = verbs.index(infinitive)
                rankings.append(ranking)
                records.append(word)
                lookup.append(infinitive)
                index.append(i)
    return rankings, records, lookup, index


def process_text(text, words):
    # split text into words
    text_list = split(text)

    # separate compound words (eg. del -> di + il)
    articles = pd.read_csv("article_split.csv", index_col="input")
    text_list = article_split(text_list, articles)

    # convert specific into generic (il -> lo, una -> uno, etc.)
    mappings = pd.concat(
        [pd.read_csv(f, index_col="specific") for f in glob("mappings/*.csv")]
    )
    text_list = strip_generic(text_list, mappings)

    # remove empty words
    text_list = strip_empty(text_list)

    # store rankings as order in the words database:
    # avere most common verb = 1, dire 2nd most common verb = 2. etc.
    rankings = {k: list() for k in words.columns}
    # store the word
    # (useful for debugging - obviously all words in original text!)
    records = {k: list() for k in words.columns}
    # store the lookup word (eg. the verb infinitive)
    records_lookup = {k: list() for k in words.columns}
    # record the index (debugging)
    records_index = {k: list() for k in words.columns}

    # Perform misc/adjectives/nouns lookups
    for key in ["locuzioni_varie", "aggettivi", "sostantivi"]:
        rankings[key], records[key], records_lookup[key], records_index[key] = rank(
            words[key], text_list
        )

    # Perform verb lookups
    # Load lookup database
    fpath_verb_mapping = "verb_mapping/*.csv"
    fpaths = sorted(glob(fpath_verb_mapping))
    dataframes = [pd.read_csv(f) for f in fpaths]
    lookup = pd.concat(dataframes)
    # get conjugations and infinitives lists
    conjugations = lookup.conjugation.to_list()
    infinitives = lookup.infinitive.to_list()
    key = "verbi"
    rankings[key], records[key], records_lookup[key], records_index[key] = rank_verbs(
        words[key], text_list, conjugations, infinitives
    )

    # check for missed words
    # TODO double counted words too
    record_sets = {k: set(idxs) for k, idxs in records_index.items()}
    used_indexes = set().union(*(s for s in record_sets.values()))
    original_indexes = set(range(len(text_list)))
    missing_indexes = original_indexes - used_indexes

    # collate data
    index = ["Word (generic form)", "Top Words Rank"]
    data = {
        k: pd.DataFrame([records_lookup[k], rankings[k]], index=index).T
        for k in rankings
    }

    # word frequencies
    # count frequency of word occurance. Sort by most to least common
    colname = "Frequency in Text"
    frequency = {
        k: d.value_counts().to_frame(colname).reset_index() for k, d in data.items()
    }

    # list of matches found
    words_found = [text_list[i] for i in used_indexes]
    words_missing = [text_list[i] for i in missing_indexes]

    return text_list, frequency, words_found, words_missing


def get_difficulty(frequency, words):
    """Print information on text difficulty level

    Parameters:
    ===========
    frequency (dict) : Word frequency / ranking database

    Notes:
    ======
    difficulty is calculated for each category, and a "total" category is created

     -> difficulty: 0 is easiest
     -> difficulty: 100 is hardest (only using 1000 most used words)
     -> difficulty > 100 is sampling outside of 1000 most used words!!

    Returns:
    ========
    difficulty (dict) : Difficulty rankings by category
    """
    difficulty = pd.Series(dtype=float)
    for key, data in frequency.items():
        # Frequency of words occurring
        freq = data["Frequency in Text"]
        freq_norm = freq / freq.sum()

        # Difficulty rank of the word
        rank = data["Top Words Rank"]
        nans = words[key].isna()
        weight = (~nans).sum()
        rank_norm = rank / weight

        # penalise missing words
        missing = nans.sum() / len(words[key])
        missing_penalty = 2
        missing *= missing_penalty

        difficulty[key] = (freq_norm * rank_norm).sum() + missing

    # unweight the 'total' difficulty
    total_weights = (~words.isna()).sum()
    difficulty["total"] = (total_weights * difficulty).sum() / total_weights.sum()

    return difficulty


def info(frequency, matches, num_words, difficulty):
    """Print information on processed text"""
    match_percent = matches / num_words * 100

    # print information on number of word matches
    print("\n# Number of Matches Found")
    print(f"Found {match_percent:.2f}% of {num_words} words* in text")
    print(" (note: combined articles such as 'al' -> a il and 'del' -> di il ")
    print("        are counted as two words)")

    # print information on most common words
    print("\n# Most common words (by category)")
    for key, frequency_data in frequency.items():
        print(f"\n## {key}")
        print(frequency_data.head())

    # print difficulty information
    print("\n# Text difficulty")
    for key, dif in difficulty.items():
        print(f"{key} {dif*100:.1f}%")


def list_to_csv(fname: str, data: list):
    """Save list to CSV file"""
    pd.Series(data).to_csv(fname, index=False)


def dict_to_excel(fname: str, data: dict, difficulty: dict):
    """Save dict to Excel sheets"""
    with pd.ExcelWriter(fname) as writer:
        for key, frame in data.items():
            frame.to_excel(writer, sheet_name=key)


def plotting(frequency: dict):
    """Generate histogram plots"""
    fig, axes_grid = plt.subplots(2, 2)
    axes = axes_grid.ravel()
    for ax, key, data in zip(axes, frequency.keys(), frequency.values()):
        xtick = range(len(data["Frequency in Text"]))
        xtick_labels = data["Frequency in Text"]
        ax.bar(xtick, data["Top Words Rank"])
        ax.set_xticklabels(xtick_labels)
        ax.set_xlabel(None)
        ax.set_ylabel(None)
        ax.set_title(key)

    for ax in axes_grid[:, 0]:
        ax.set_ylabel("Top Words Rank")
    for ax in axes_grid[-1, :]:
        ax.set_xlabel("Frequency in Text")

    fig.tight_layout()
    plt.show()


def get_args():
    """parse command line arguments"""
    title = "Parole Più Usate"
    description = (
        "Scan a text file to lookup the distribution of common/uncommon "
        "words in Italian language."
    )
    parser = argparse.ArgumentParser(description=f"{title}:\n{description}")
    parser.add_argument("fname", type=str, help="filepath to input text")
    parser.add_argument(
        "-x", "--excel", action="store_true", help="export to excel sheets"
    )
    parser.add_argument(
        "-p", "--plot", action="store_true", help="interactive plotting"
    )
    parser.add_argument(
        "-i",
        "--info",
        action="store_true",
        help="print summary info such as percentage of words identified",
    )
    parser.add_argument(
        "--missing",
        action="store_true",
        help="(for debugging/improving the engine) save missing words list",
    )

    # quiet/verbose/debug
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-q", "--quiet", action="store_true")
    group.add_argument("-v", "--verbose", action="store_true")
    group.add_argument("-d", "--debug", action="store_true")

    return parser.parse_args()


def main():
    # get command line arguments
    args = get_args()

    # setup logging
    logging_format = "%(asctime)s %(message)s"
    logging.basicConfig(format=logging_format)
    logger = logging.getLogger()

    # quiet/verbose/debug
    if args.quiet:
        logger.setLevel(40)
    if args.verbose:
        logger.setLevel(20)
    if args.debug:
        logger.setLevel(10)

    # check that at least one useful arg passed (otherwise no point continuing)
    if not any((args.excel, args.info, args.missing, args.debug)):
        logger.warning("At least one of [-x, -i, --missing] is required")
        raise SystemExit(0)

    # text to search through and rank word difficulty
    fname_base = args.fname.rsplit(".", 1)[0]
    logger.info("Opening text file %s" % args.fname)
    with open(args.fname, "r") as read_in:
        text = read_in.read().replace("\n", " ")

    # list(s) of words, nouns / adjectives / verbs / misc.
    fname_words = "parole.csv"
    logger.info("Reading words list {fname_words}")
    words = pd.read_csv(fname_words)

    # process the text
    logger.info("Processing text")
    text_list, frequency, words_found, words_missing = process_text(text, words)

    # calculate the difficulty
    difficulty = get_difficulty(frequency, words)

    # Print processing info
    if args.info:
        info(frequency, len(words_found), len(text_list), difficulty)

    # Save missing words list
    if args.missing:
        fname = fname_base + "_missing_words.csv"
        logger.info("Save missing words list to {fname}")
        list_to_csv(fname, words_missing)

    # Save frequency data to excel
    if args.excel:
        fname = fname_base + "_word_frequency.xlsx"
        logger.info("Saving output to %s" % fname)
        dict_to_excel(fname, frequency, difficulty)

    # Histogram plotting
    if args.plot:
        plotting(frequency)


if __name__ == "__main__":
    main()
