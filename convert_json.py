"""Generate verb mappings (eg. avendo -> avere, vado -> andare)"""
import json
from glob import glob

import pandas as pd


def get_values(conjugations: list[dict]) -> tuple[list]:
    """Get conjugations and their infintive from dict item"""
    # list of different conjugations
    con_list, inf_list = list(), list()
    infinitive = conjugations[0]["value"].strip("\n")

    # get entry from conjugations list
    for full_entry in conjugations:
        # ignore auxiliary verbs avere/essere
        if full_entry["group"] != "auxiliaryverb":
            # split entry after each comma (sometimes multiple words listed)
            for entry in full_entry["value"].split(","):
                # Get conjugation, strip newline, get last word
                split_entry = entry.strip("\n ").split(" ")[-1]
                # append final extracted word (usually just one) to save list
                if len(split_entry) > 1:
                    con_list.append(split_entry)
                    inf_list.append(infinitive)

    # return conjugations and infinitive mappings
    return con_list, inf_list


def process_verb_data(fpath: str) -> tuple[list]:
    """Convert JSON verb-data into reverse-looup items

    Parameters:
    ===========
    fpath (str): Path to file, eg. "verb-data-italian/content/a/avere.json"
    """
    # get json data
    with open(fpath, "r") as j:
        contents = json.loads(j.read())
    conjugations = contents["conjugations"]

    # only get values and return if not none
    if conjugations is not None:
        return get_values(contents["conjugations"])


def verb_mapping(letter):
    # list of verb files
    verb_files = glob(f"verb-data-italian/content/{letter}/*.json")

    # loop over files and get the conjugation/infinitive list
    con_list, inf_list = list(), list()
    for fpath in verb_files:
        processed = process_verb_data(fpath)
        if processed is not None:
            con_list += processed[0]
            inf_list += processed[1]

    verb_mapping = pd.DataFrame(
        [con_list, inf_list], index=["conjugation", "infinitive"]
    ).T

    verb_mapping.to_csv(f"verb_mapping/{letter}.csv", index=False)


def main():
    for letter in list("abcdefghijklmnopqrstuvwxyz"):
        verb_mapping(letter)


if __name__ == "__main__":
    main()
